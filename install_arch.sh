#!/bin/bash

timedatectl set-ntp true
mount /dev/sda4 /mnt
mkdir /mnt/home
mount /dev/sda3 /mnt/home
mkdir /mnt/boot
mount /dev/sda1 /mnt/boot

# noto-fonts-cjk    Chinese fonts
# sc                Terminal spreadsheet editor

pacstrap /mnt base linux linux-firmware vim man-db man-pages texinfo xterm xorg-server xorg-xrandr xorg-xinit sudo xmonad xmonad-contrib xmobar xf86-video-intel netctl dhcpcd wpa_supplicant dialog dmenu alsa-utils git autorandr feh redshift wget vifm zathura-pdf-poppler xcape acpi xbindkeys base-devel picom brightnessctl xorg-xset openssh dnsutils unzip nextcloud-client firefox vlc noto-fonts-cjk sc youtube-dl cmus

genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt
ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime
hwclock --systohc
